// +----------------------------------------------------------------------
// | JavaWeb_Cloud_EleVue_Pro微服务前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.service.system.controller;


import com.javaweb.common.framework.common.BaseController;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.service.system.entity.Config;
import com.javaweb.service.system.query.ConfigQuery;
import com.javaweb.service.system.service.IConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 配置表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-06
 */
@RestController
@RequestMapping("/config")
public class ConfigController extends BaseController {

    @Autowired
    private IConfigService configService;

    /**
     * 获取配置列表
     *
     * @param configGroupQuery 查询条件
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:configgroup:index')")
    @GetMapping("/index")
    public JsonResult index(ConfigQuery configGroupQuery) {
        return configService.getList(configGroupQuery);
    }

    /**
     * 添加配置
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:configgroup:add')")
    @PostMapping("/add")
    public JsonResult add(@RequestBody Config entity) {
        return configService.edit(entity);
    }

    /**
     * 编辑配置
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:configgroup:edit')")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody Config entity) {
        return configService.edit(entity);
    }

    /**
     * 删除配置
     *
     * @param configId 配置ID
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:configgroup:delete')")
    @DeleteMapping("/delete/{configId}")
    public JsonResult delete(@PathVariable("configId") Integer configId) {
        return configService.deleteById(configId);
    }

}
