// +----------------------------------------------------------------------
// | JavaWeb_EleVue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2021 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <javaweb520@gmail.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.service.system.controller;

import com.javaweb.common.framework.common.BaseController;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.service.system.entity.Example;
import com.javaweb.service.system.query.ExampleQuery;
import com.javaweb.service.system.service.IExampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 演示案例一 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2021-10-29
 */
@RestController
@RequestMapping("/example")
public class ExampleController extends BaseController {

    @Autowired
    private IExampleService exampleService;

    /**
     * 获取数据列表
     *
     * @param exampleQuery 查询条件
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:example:index')")
    @GetMapping("/index")
    public JsonResult index(ExampleQuery exampleQuery) {
        return exampleService.getList(exampleQuery);
    }

    /**
     * 添加记录
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:example:add')")
    @PostMapping("/add")
    public JsonResult add(@RequestBody Example entity) {
        return exampleService.edit(entity);
    }

    /**
     * 获取详情
     *
     * @param exampleId 记录ID
     * @return
     */
    @GetMapping("/info/{exampleId}")
    public JsonResult info(@PathVariable("exampleId") Integer exampleId) {
        return exampleService.info(exampleId);
    }

    /**
     * 更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:example:edit')")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody Example entity) {
        return exampleService.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param exampleIds 记录ID
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:example:delete')")
    @DeleteMapping("/delete/{exampleIds}")
    public JsonResult delete(@PathVariable("exampleIds") Integer[] exampleIds) {
        return exampleService.deleteByIds(exampleIds);
    }

    /**
     * 设置是否VIP
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:example:isVip')")
    @PutMapping("/setIsVip")
    public JsonResult setIsVip(@RequestBody Example entity) {
        return exampleService.setIsVip(entity);
    }
    /**
     * 设置状态
     *
     * @param entity 实体对象
     * @return
     */
    @PreAuthorize("@jw.hasPermission('sys:example:status')")
    @PutMapping("/setStatus")
    public JsonResult setStatus(@RequestBody Example entity) {
        return exampleService.setStatus(entity);
    }
}